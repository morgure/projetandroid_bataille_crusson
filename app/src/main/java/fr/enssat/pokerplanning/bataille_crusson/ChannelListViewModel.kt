package fr.enssat.pokerplanning.bataille_crusson

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.json.JSONObject

class ChannelListViewModel(context: Context): ViewModel() {

    val channelsStorage = ChannelsStorage(context)
    private val _msgSet = mutableSetOf<String>()
    private val _allchannels = MutableLiveData<List<String>>()
    private val _multicast = MultiCastAgent(context, this::onReceiveMessage)
    private val _socketserver = ServerSocket(context)

    val channels: LiveData<List<String>> get() = _allchannels

    init {
        MultiCastAgent.wifiLock(context)
        _multicast.startReceiveLoop()
        if(channelsStorage.channelActiveState()){
            _socketserver.startListening()
        }
    }

    override fun onCleared() {
        _multicast.stopReceiveLoop()
        //_socketserver.stopListening()
        MultiCastAgent.releaseWifiLock()
        super.onCleared()
    }

    fun onReceiveMessage(msg: String) {
        if(msg.equals("UPDATE")) {
            if(channelsStorage.channelActiveState()){
                _multicast.sendChannel(channelsStorage.returnChannel("0"))
            }
            for(x in 1 until channelsStorage.lastChannelId()+1){
                _multicast.sendChannel(channelsStorage.returnChannel(x.toString()))
            }
        }
        else{
            var new = true
            var jsonMsg = JSONObject(msg)
            _msgSet.forEach {
                var jsonValue = JSONObject(it)
                if(jsonMsg.getString("uuid") == jsonValue.getString("uuid")){
                    new = false
                }
            }
            if(new){
                _msgSet.add(msg)
                _allchannels.postValue(_msgSet.toList())
            }
        }
    }

    fun send(msg: String) {
        _multicast.send(msg)
    }

    fun releaseAllChannels(){
        _msgSet.clear()
        _allchannels.postValue(_msgSet.toList())
    }
}
package fr.enssat.pokerplanning.bataille_crusson

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import org.json.JSONObject


class MessageViewHolder(val view: View): RecyclerView.ViewHolder(view) {
    val channelInfoBriefView = view.findViewById<TextView>(R.id.channel_info_brief)

    fun setMessage(channel: String, listener: (String) -> Unit) {
        var jsonChannel = JSONObject(channel)
        if(jsonChannel.getString("id") == "0")
        {
            channelInfoBriefView.text = "Sondage : " + jsonChannel.getString("sondage") + " (actif)" + "\nby " + jsonChannel.getString("autor")
        }
        else{
            channelInfoBriefView.text = "Sondage : " + jsonChannel.getString("sondage") + " (fermé)" + "\nby " + jsonChannel.getString("autor")
        }
        channelInfoBriefView.setOnClickListener { view ->
            //val intent = Intent(view.context, ChannelDetailsActivity::class.java)
            //startActivity(view.context,intent,null)
            Log.d("DEBUG", "onItemClick : " + jsonChannel.getString("sondage"))
            listener(channel)
        }
    }
}

class MessageAdapter(val listener: (String) -> Unit): RecyclerView.Adapter<MessageViewHolder>() {

    var list: List<String> = emptyList()
        set(l) {
            field = l
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.channel_brief, parent, false)
        return MessageViewHolder(view)
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        holder.setMessage(list[position], listener)
    }
}
package fr.enssat.pokerplanning.bataille_crusson

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import fr.enssat.pokerplanning.bataille_crusson.databinding.ActivityChannelListBinding


class ChannelListActivity : AppCompatActivity() {
    lateinit var binding: ActivityChannelListBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_channel_list)

        val model = ViewModelProviders.of(this, ChannelListViewModelFactory(this)).get(ChannelListViewModel::class.java)

        model.releaseAllChannels()
        model.send("UPDATE")

        binding.channelFab.setOnClickListener {
            viewModelStore.clear()
            val intent = Intent(this, MyChannelActivity::class.java)
            startActivity(intent)
            //finish()
        }
        binding.refreshButton.setOnClickListener {
            model.releaseAllChannels()
            model.send("UPDATE")
        }

        val adapter = MessageAdapter(this::onClick)

        binding.channelList.adapter = adapter

        model.channels.observe(this, Observer { list ->
            adapter.list = list
        })
    }

    fun onClick(channel: String){
        viewModelStore.clear()
        val intent = Intent(this, ChannelDetailsActivity::class.java)

        intent.putExtra("channel",channel)
        startActivity(intent)
        //finish()
    }


    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_channel_list)

        val model = ViewModelProviders.of(this, ChannelListViewModelFactory(this)).get(ChannelListViewModel::class.java)

        model.releaseAllChannels()
        model.send("UPDATE")
    }


}

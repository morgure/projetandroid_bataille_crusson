package fr.enssat.pokerplanning.bataille_crusson

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.json.JSONObject

class ChannelDetailsViewModel(context: Context): ViewModel() {
    val channelsStorage = ChannelsStorage(context)
    private val _multicast = MultiCastAgent(context, this::onReceiveMessage)
    private val _socketserver = ServerSocket(context)
    private val _socketclient = ClientSocket(context, this::onReceiveReply)
    var msgReply  = MutableLiveData<String>()

    init {
        MultiCastAgent.wifiLock(context)
        _multicast.startReceiveLoop()
        if(channelsStorage.channelActiveState()){
            _socketserver.startListening()
        }
    }

    override fun onCleared() {
        _multicast.stopReceiveLoop()
        //_socketserver.stopListening()

        MultiCastAgent.releaseWifiLock()
        super.onCleared()
    }

    fun onReceiveMessage(msg: String) {
        if(msg.equals("UPDATE")) {
            if(channelsStorage.channelActiveState()){
                _multicast.sendChannel(channelsStorage.returnChannel("0"))
            }
            for(x in 1 until channelsStorage.lastChannelId()+1){
                _multicast.sendChannel(channelsStorage.returnChannel(x.toString()))
            }
        }
    }

    fun myChannelState() :Boolean{
        return channelsStorage.channelActiveState()
    }

    fun connect(serverIp:String){
        _socketclient.connect(serverIp)
    }

    fun sendVote(vote :String){
        var jsonVote = JSONObject()
        jsonVote.put("id","VOTE")
        jsonVote.put("value",vote)
        _socketclient.sendAndReceive(jsonVote.toString())
    }

    fun onReceiveReply(reply: String){
        var jsonMsg = JSONObject(reply)
        if(jsonMsg.getString("id") == "VOTE"){
            if(jsonMsg.getString("value") == "OK"){
                msgReply.postValue("Vote comptabilisé")
            }
            else if(jsonMsg.getString("value") == "NOK"){
                msgReply.postValue("Vote non comptabilisé")
            }
        }
        else{
            msgReply.postValue("Une erreur s'est produite")
        }
    }
}
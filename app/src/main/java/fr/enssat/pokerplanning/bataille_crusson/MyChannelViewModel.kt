package fr.enssat.pokerplanning.bataille_crusson

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.json.JSONObject

class MyChannelViewModel(context: Context): ViewModel() {
    val channelsStorage = ChannelsStorage(context)


    private val _multicast = MultiCastAgent(context,this::onReceiveMessage)
    private val _socketserver = ServerSocket(context)

    init {
        MultiCastAgent.wifiLock(context)
        _multicast.startReceiveLoop()
        if(channelsStorage.channelActiveState()){
            _socketserver.startListening()
        }
    }

    override fun onCleared() {
        _multicast.stopReceiveLoop()
        //_socketserver.stopListening()
        MultiCastAgent.releaseWifiLock()
        super.onCleared()
    }
    fun onReceiveMessage(msg: String) {
        if(msg.equals("UPDATE")) {
            if(channelsStorage.channelActiveState()){
                _multicast.sendChannel(channelsStorage.returnChannel("0"))
            }
            for(x in 1 until channelsStorage.lastChannelId()+1){
                _multicast.sendChannel(channelsStorage.returnChannel(x.toString()))
            }
        }
    }

    fun openChannel(sondage_name :String) :Boolean{
        channelsStorage.openChannel(sondage_name)
        return _multicast.sendChannel(channelsStorage.returnChannel("0"))
    }
    fun closeChannel()
    {
        channelsStorage.closeChannel()
    }
    fun myChannelState() :Boolean{
        return channelsStorage.channelActiveState()
    }
    fun getChannelName() :String{
        var channel = channelsStorage.returnChannel("0")
        return channel.get("sondage").toString()
    }
}
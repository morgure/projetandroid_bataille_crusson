package fr.enssat.pokerplanning.bataille_crusson

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import org.json.JSONObject
import java.io.IOException
import java.net.ServerSocket
import java.net.Socket
import java.nio.charset.StandardCharsets
import java.util.concurrent.Executor
import java.util.concurrent.Executors


class ServerSocket(val context: Context) {
    private val TAG = this.javaClass.simpleName


    companion object {
        val PORT = 6666
        private lateinit var serverSocket: ServerSocket
        private var STATE = false
    }


    private val IPADDRESS = NetworkUtils.getIpAddress(context)
    private val MAX_CLIENT_BACK_LOG = 50
    private var loop = true

    //liste des clients connectés au serveur
    private val allClientsConnected = mutableListOf<Reply>()

    fun startListening() {
        if(!STATE){
            STATE = true
            serverSocket = ServerSocket(PORT, MAX_CLIENT_BACK_LOG, IPADDRESS)
            Log.d(TAG, "Start serversocket")
            Log.d(TAG, "IP" + IPADDRESS.toString())

            // écoute toutes les nouvelles demandes de connections clientes
            // et crée une socket locale au serveur, reply dédiée a ce nouveau client.
            Executors.newSingleThreadExecutor().execute {
                try{
                    while (loop) {
                        val newSocket = serverSocket.accept()
                        allClientsConnected.add(Reply(context, newSocket))
                    }
                } catch (e: IOException) {
                }
            }
        }
    }

    fun stopListening() {
        if(STATE){
            STATE = false
            loop = false
            allClientsConnected.forEach { it.stop() }
            serverSocket.close()
            Log.d(TAG, "Stop serverSocket")
        }

    }


    class Reply(val context: Context, val socket: Socket) {
        val TAG = this.javaClass.simpleName
        var loop = true
        val address = socket.inetAddress.address.toString()
        val channelsStorage = ChannelsStorage(context)

        private val mainThread = object : Executor {
            val handler = Handler(Looper.getMainLooper())
            override fun execute(command: Runnable) {
                handler.post(command)
            }
        }

        init {
            Executors.newSingleThreadExecutor().execute {
                try {
                    while (loop) {
                        val buffer = ByteArray(2048)
                        val len = socket.getInputStream().read(buffer)
                        val msg = String(buffer, 0, len, StandardCharsets.UTF_8)
                        //send ack
                        var jsonMsg = JSONObject(msg)
                        var reply = JSONObject().put("id","ERR")
                        if(jsonMsg.getString("id") == "VOTE") {
                            if(channelsStorage.channelActiveState()){
                                channelsStorage.updateChannelVote(jsonMsg.getString("value"))
                                reply = JSONObject().put("id","VOTE").put("value","OK")
                            }
                            else{
                                reply = JSONObject().put("id","VOTE").put("value","NOK")
                            }
                        }
                        else if(jsonMsg.getString("id") == "CLOSE") {
                            reply = JSONObject().put("id","CLOSE").put("value","ACK")
                        }

                        val data = reply.toString()
                            .toByteArray(StandardCharsets.UTF_8)
                        socket.getOutputStream().write(data)
                        socket.getOutputStream().flush()
                        if(JSONObject(msg).getString("id") == "CLOSE"){
                            stop()
                        }
                    }
                } catch (e: IOException) {
                    Log.d(TAG, "Client $address down")
                }
            }
        }

        fun stop() {
            loop = false
            socket.close()
        }
    }
}

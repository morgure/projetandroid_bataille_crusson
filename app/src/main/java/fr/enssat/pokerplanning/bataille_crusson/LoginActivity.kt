package fr.enssat.pokerplanning.bataille_crusson

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import fr.enssat.pokerplanning.bataille_crusson.databinding.ActivityLoginBinding


class LoginActivity : AppCompatActivity() {
    lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        val model = ViewModelProviders.of(this, LoginViewModelFactory(this)).get(LoginViewModel::class.java)

        binding.pseudoText.setText(model.getPseudo())
        binding.pseudoButton.setOnClickListener{ view->
            if(binding.pseudoText.text.toString() != "") {

                model.setPseudo(binding.pseudoText.text.toString())

                val intent = Intent(this, ChannelListActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }
}


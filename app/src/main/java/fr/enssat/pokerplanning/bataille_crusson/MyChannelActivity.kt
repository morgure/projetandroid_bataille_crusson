package fr.enssat.pokerplanning.bataille_crusson

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import fr.enssat.pokerplanning.bataille_crusson.databinding.ActivityMyChannelBinding


class MyChannelActivity : AppCompatActivity() {
    lateinit var binding: ActivityMyChannelBinding




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_channel)
        val model = ViewModelProviders.of(this, MyChannelViewModelFactory(this)).get(MyChannelViewModel::class.java)

        if(model.myChannelState()){
            binding.channelName.setText(model.getChannelName())
            binding.channelName.isFocusable = false
            binding.channelButton.setText("Fermer le vote")
        }

        binding.channelButton.setOnClickListener{ view ->
            if(model.myChannelState()){
                model.closeChannel()
            }
            else{
                model.openChannel(binding.channelName.text.toString())
            }
            Thread.sleep(100)
            viewModelStore.clear()
            val intent = Intent(this, ChannelListActivity::class.java)
            startActivity(intent)
            //finish()
        }

        binding.cancelButton.setOnClickListener { view ->

            viewModelStore.clear()
            val intent = Intent(this, ChannelListActivity::class.java)
            startActivity(intent)
            //finish()

        }
    }
}

package fr.enssat.pokerplanning.bataille_crusson

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.ViewModel

class LoginViewModel(context: Context): ViewModel() {
    val PREFS_APP_FILENAME = "fr.enssat.pokerplanning.bataille_crusson.app"
    var appPref = context.getSharedPreferences(PREFS_APP_FILENAME, Context.MODE_PRIVATE)
    val editor: SharedPreferences.Editor = appPref.edit()



    fun setPseudo(pseudo :String){

        editor.putString("autor", pseudo).commit()
    }

    fun getPseudo() :String {
        return appPref.getString("autor","Anonymous").toString()
    }

}


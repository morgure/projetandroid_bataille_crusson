package fr.enssat.pokerplanning.bataille_crusson

import android.content.Context
import android.content.SharedPreferences
import org.json.JSONObject
import java.util.*


class ChannelsStorage(context: Context) {
    val PREFS_MY_CHANNELS_FILENAME = "fr.enssat.pokerplanning.bataille_crusson.mychannels"
    val PREFS_APP_FILENAME = "fr.enssat.pokerplanning.bataille_crusson.app"
    var myChannelsPref = context.getSharedPreferences(PREFS_MY_CHANNELS_FILENAME, Context.MODE_PRIVATE)
    var appPref = context.getSharedPreferences(PREFS_APP_FILENAME, Context.MODE_PRIVATE)

    fun openChannel(sondage_name: String){
        var editor: SharedPreferences.Editor = myChannelsPref.edit()
        var channel = JSONObject()
        channel.put("uuid", UUID.randomUUID().toString())
        channel.put("id","0")
        channel.put("autor",appPref.getString("autor","Anonymous"))
        channel.put("sondage",sondage_name)
        channel.put("1",0)
        channel.put("2",0)
        channel.put("5",0)
        channel.put("10",0)
        channel.put("50",  0)
        editor.putString("0",channel.toString()).commit()
        channelActiveStateChange()

    }

    fun closeChannel(){
        var editor: SharedPreferences.Editor = myChannelsPref.edit()
        var channel = returnChannel("0")
        var id = lastChannelId()+1
        channel.put("id",id.toString())
        editor.putString(id.toString(),channel.toString()).commit()
        updateChannelId()
        editor.remove("0").commit()
        channelActiveStateChange()
    }

    fun returnChannel(id: String): JSONObject{
        var channel = JSONObject(myChannelsPref.getString(id,null))
        return channel
    }

    fun channelActiveState(): Boolean{
        return myChannelsPref.getBoolean("channelActiveState",false)
    }

    fun channelActiveStateChange(){
        var editor: SharedPreferences.Editor = myChannelsPref.edit()
        if(channelActiveState()){
            editor.putBoolean("channelActiveState",false).commit()
        }
        else{
            editor.putBoolean("channelActiveState",true).commit()
        }
    }

    fun lastChannelId(): Int{
        return myChannelsPref.getInt("lastID",0)
    }

    fun updateChannelId(){
        var editor: SharedPreferences.Editor = myChannelsPref.edit()
        var id = lastChannelId() + 1
        editor.putInt("lastID",id).commit()
    }

    fun updateChannelVote(response: String){
        var editor: SharedPreferences.Editor = myChannelsPref.edit()
        var channel = returnChannel("0")
        channel.put(response, channel.getInt(response)+1)
        editor.putString("0",channel.toString()).commit()


    }
}
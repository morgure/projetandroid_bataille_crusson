package fr.enssat.pokerplanning.bataille_crusson

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import fr.enssat.pokerplanning.bataille_crusson.databinding.ActivityChannelDetailsBinding
import org.json.JSONObject


class ChannelDetailsActivity : AppCompatActivity() {
    private val TAG = this.javaClass.simpleName
    lateinit var binding: ActivityChannelDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val channel = JSONObject(intent.getStringExtra("channel"))
        binding = DataBindingUtil.setContentView(this, R.layout.activity_channel_details)
        val model = ViewModelProviders.of(this, ChannelDetailsViewModelFactory(this)).get(ChannelDetailsViewModel::class.java)

        model.connect(channel.getString("ip"))
        binding.channelName.setText( "Sondange : " + channel.getString("sondage"))
        if(channel.getInt("id") == 0){
            binding.radioGroup.visibility = View.VISIBLE
            binding.submitButton.visibility = View.VISIBLE
            binding.text1.visibility = View.INVISIBLE
            binding.text2.visibility = View.INVISIBLE
            binding.text5.visibility = View.INVISIBLE
            binding.text10.visibility = View.INVISIBLE
            binding.text50.visibility = View.INVISIBLE
        }
        else{
            binding.text1.visibility = View.VISIBLE
            binding.text2.visibility = View.VISIBLE
            binding.text5.visibility = View.VISIBLE
            binding.text10.visibility = View.VISIBLE
            binding.text50.visibility = View.VISIBLE
            binding.text1.text = "1 : " + channel.getInt("1").toString()
            binding.text2.text = "2 : " + channel.getInt("2").toString()
            binding.text5.text = "5 : " + channel.getInt("5").toString()
            binding.text10.text = "10 : " + channel.getInt("10").toString()
            binding.text50.text = "50 : " + channel.getInt("50").toString()
        }

        binding.cancelButton.setOnClickListener {
            viewModelStore.clear()
            val intent = Intent(this, ChannelListActivity::class.java)
            startActivity(intent)
            //finish()
        }
        binding.submitButton.setOnClickListener {
            if(binding.radioButton1.isChecked){
                model.sendVote("1")
                binding.submitButton.visibility = View.INVISIBLE
            }
            else if(binding.radioButton2.isChecked){
                model.sendVote("2")
                binding.submitButton.visibility = View.INVISIBLE
            }
            else if(binding.radioButton5.isChecked){
                model.sendVote("5")
                binding.submitButton.visibility = View.INVISIBLE
            }
            else if(binding.radioButton10.isChecked){
                model.sendVote("10")
                binding.submitButton.visibility = View.INVISIBLE
            }
            else if(binding.radioButton50.isChecked){
                model.sendVote("50")
                binding.submitButton.visibility = View.INVISIBLE
            }
        }

        model.msgReply.observe(this, Observer {
            binding.networkTxt.setText(it)
        })
    }
}


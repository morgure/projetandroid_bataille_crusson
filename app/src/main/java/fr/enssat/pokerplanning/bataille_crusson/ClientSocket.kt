package fr.enssat.pokerplanning.bataille_crusson

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import org.json.JSONObject
import java.io.IOException
import java.net.InetAddress
import java.net.Socket
import java.nio.charset.StandardCharsets
import java.util.concurrent.Executor
import java.util.concurrent.Executors


class ClientSocket(val context: Context, val messageListener: (String) -> Unit) {
    private val TAG = this.javaClass.simpleName

    private lateinit var socket: Socket

    private val mainThread = object : Executor {
        val handler = Handler(Looper.getMainLooper())
        override fun execute(command: Runnable) {
            handler.post(command)
        }
    }

    fun connect(serverIp:String) {
        Executors.newSingleThreadExecutor().execute {
            try {
                val address = InetAddress.getByName(serverIp)
                val port = 6666
                Log.d(TAG, "try connecting to $address:$port")
                socket = Socket(address, port)
                //sendAndReceive("coucou")
            }
            catch(ex:Exception) {
                val msg = ex.message ?: "unable to connect server"
                Log.d(TAG, msg)
            }
        }
    }

    fun sendAndReceive(msg: String) {
        Executors.newSingleThreadExecutor().execute {
            try {
                //envoi
                val data = msg.toByteArray(StandardCharsets.UTF_8)
                socket.getOutputStream().write(data)
                socket.getOutputStream().flush()
                //reception
                val buffer = ByteArray(2048)
                val len = socket.getInputStream().read(buffer)
                val str = String(buffer, 0, len, StandardCharsets.UTF_8)


                var jsonMsg = JSONObject(str)
                if(jsonMsg.getString("id") == "VOTE")
                {
                    mainThread.execute { messageListener(str) }
                    sendAndReceive(JSONObject().put("id","CLOSE").toString())
                }
                else if(jsonMsg.getString("id") == "CLOSE")
                {
                    socket.close()
                }


            } catch (e: IOException) {
                val msg = e.message ?: "unable to send to or receive from server"
                Log.d(TAG, msg)
                sendAndReceive(JSONObject().put("id","CLOSE").toString())
            }
        }
    }
}
